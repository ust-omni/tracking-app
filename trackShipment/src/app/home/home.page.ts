import { ThrowStmt } from "@angular/compiler";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { ToastController, Platform, AlertController } from "@ionic/angular";
import { RouterModule } from "@angular/router";
import { Router } from "@angular/router";
import { UniqueDeviceID } from "@ionic-native/unique-device-id/ngx";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";
import { Uid } from "@ionic-native/uid/ngx";

import { Component } from "@angular/core";
import { RegisterDeviceService } from "../../register-device.service";
@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  devicedetails: any;
  deviceUUID: any;
  UniqueDeviceID: string;
  deviceID: any;

  constructor(
    private register: RegisterDeviceService,
    public alertController: AlertController,
    public router: Router,
    private uniqueDeviceID: UniqueDeviceID,
    private uid: Uid,
    private androidPermissions: AndroidPermissions
  ) {}

  ngOnInit() {
    this.uniqueDeviceID.get()
    .then((uuid: any) => {
      this.alertController.create({
        header: 'Alert',
        message: "Unique Device Id is" + " " + uuid,
        buttons: ['OK']
      }).then(res => {
        res.present();
      });
      this.deviceUUID = uuid;
    }
      )
    .catch((error: any) => console.log(error));
  }

  //Register the Device
  registerDevice() {
    var data = { uniqueId: this.deviceUUID }; //Random UUID
    //var data = { "uniqueId": '29963556' } //Static UUID
    this.register.registerDevice(data).subscribe(
      (res) => {
        if (res.code == 201) {
          console.log(res);
          //alert(res.msg);
          this.alertController
            .create({
              header: "Alert",
              message: res.msg,
              buttons: ["OK"],
            })
            .then((res) => {
              res.present();
            });
          this.devicedetails = res.response;
          console.log(this.devicedetails);
          this.deviceID = this.devicedetails.deviceId;
          console.log(this.deviceID);
        }
        //alert(res.msg);
        this.alertController
          .create({
            header: "Alert",
            message: res.msg,
            buttons: ["OK"],
          })
          .then((res) => {
            res.present();
          });
        this.router.navigate(["/details"]);
      },

      (error) => {
        console.log(error);
      }
    );
  }
}
