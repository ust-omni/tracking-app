import {
  Component,
  ElementRef,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
} from "@angular/core";
import { RegisterDeviceService } from "src/register-device.service";
import {
  Geolocation,
  GeolocationOptions,
  Geoposition,
  PositionError,
} from "@ionic-native/geolocation/ngx";
import { AlertController } from "@ionic/angular";
import { NativeGeocoder } from "@ionic-native/native-geocoder/ngx";
declare var google;

@Component({
  selector: "app-details",
  templateUrl: "./details.page.html",
  styleUrls: ["./details.page.scss"],
})
export class DetailsPage implements OnInit {
  @ViewChild("map", { static: false }) mapElement: ElementRef;
  map: any;
  address: string;

  latitude: number = 0;
  longitude: number = 0;
  todayISOString: string;

  isEnabled: boolean = false;

  polling: any;
  selectedData: any;
  interval: any;

  constructor(
    private register: RegisterDeviceService,
    public alertController: AlertController,
    private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public changeRef: ChangeDetectorRef
  ) {}

  ngOnInit() {}

  getLocationData() {
    let options = {
      enableHighAccuracy: true,
    };
    this.geolocation
      .getCurrentPosition(options)
      .then((position) => {
        //position.coords.longitude;
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.todayISOString = new Date().toISOString();
        this.changeRef.detectChanges();
        var dataParam = {
          device: { deviceId: "1" },
          latitude: this.latitude,
          longitude: this.longitude,
          datetime: this.todayISOString,
        };
        this.loadMap(this.latitude, this.longitude);
        this.register.updateLocation(dataParam).subscribe((res) => {
          if (res === 201) {
            console.log(res);
          }
        });
      })
      .catch((error) => {
        console.log("Error getting location", error);
      });
  }

  setLocationInterval(interval) {
    this.isEnabled = !this.isEnabled;
    this.polling = setInterval(() => {
      this.alertController.create({
        header: 'Alert',
        message: "Location getting updated",
        buttons: ['OK']
      }).then(res => {
        res.present();
      });
      this.getLocationData();
    }, interval * 60000);
  }

  //Show Prompt for location update interval
  getLocation() {
    this.alertController
      .create({
        header: "Select Interval",
        message: "Location will get Updated At set interval",
        inputs: [
          {
            type: "radio",
            label: "2-min",
            value: 2,
          },
          {
            type: "radio",
            label: "5-min",
            value: 5,
          },
          {
            type: "radio",
            label: "10-min",
            value: 10,
          },
        ],
        buttons: [
          {
            text: "Cancel",
            handler: (data: any) => {
              this.getLocationData();
              this.setLocationInterval(2);
              //console.log('Canceled', data);
            },
          },
          {
            text: "Done!",
            handler: (data: any) => {
              console.log("Selected Information", data);
              this.getLocationData();
              this.setLocationInterval(data);
              // this.getLocationData();
              // this.polling = setInterval(() => {
              //   this.getLocationData();
              // }, this.selectedData * 60000)
            },
          },
        ],
      })
      .then((res) => {
        res.present();
      });
  }

  loadMap(lat, lng) {
    this.latitude = lat;
    this.longitude = lng;

    let latLng = new google.maps.LatLng(lat, lng);
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    // tslint:disable-next-line: no-unused-expression
    new google.maps.Marker({
      position: { lat, lng },
      map: this.map,
      title: "Latitude:" + this.latitude + " Longitude:" + this.longitude,
    });

    this.map.addListener("dragend", () => {
      this.latitude = this.map.center.lat();
      this.longitude = this.map.center.lng();
    });
  }

  ngOnDestroy() {
    clearInterval(this.polling);
  }
}
