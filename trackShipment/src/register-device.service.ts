import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './environments/environment';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { UniqueDeviceID } from '@ionic-native/unique-device-id/ngx';

@Injectable({
  providedIn: 'root'
})
export class RegisterDeviceService {
  private API_URL = environment.apiUrl;

  devicetData: any[] = [];
  constructor(private http: HttpClient,private uniqueDeviceID: UniqueDeviceID,private geolocation: Geolocation) { }

  registerDevice(data): Observable<any>  {
    //ApiUrl Call once api are ready
    const url: string = this.API_URL + '/api/registerDevice';
    return this.http.post(url, data);
  
   // return this.http.get('../assets/deviceRegisterData.json')  //Dummy Api call 
  }


  updateLocation(dataParam){
     const url: string = this.API_URL + '/api/registerDeviceLocation';
    return this.http.post(url, dataParam);
  }

}

